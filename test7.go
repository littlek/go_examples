package main

import "fmt"

var x, y, z int = 1, 2, 3
var c, python, java = false, 3.0, "NONO"

func main() {
	fmt.Println(x, y, z, c, python, java)
}
