package main

import "fmt"

type Vertex struct {
	X int
	Y int
}

func main() {
	v := new(Vertex)
	fmt.Println(v)
	v.X, v.Y = 12, 20
	fmt.Println(v)
}
